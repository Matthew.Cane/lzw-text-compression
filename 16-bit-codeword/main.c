#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

typedef struct _dictionaryEntry
{
	unsigned int codeword : 16;
	char character[50];
} dictionaryEntry;

const int arraySize = 65536; //Constant for array size, 2 ^ 16

bool isInDictionary(char * character, dictionaryEntry * dictionary);
void addToDictionary(char * character, dictionaryEntry * dictionary);
void addToOutput(char * character, dictionaryEntry * dictionary, FILE * outputFilePointer);
void intitaliseDictionary(dictionaryEntry * dictionary);
void setToNull(char * string);
FILE * getInputFilePointer(char * fileName);
int fileSize(FILE * inputFilePointer);

int main(int argc, char * argv[])
{
	setbuf(stdout, NULL); //Sets the buffer for stdout to NULL, avoiding having to flush after each print
	char fileName[50];

	if (argc != 2)
	{
		printf("ERROR.\nLZW takes one input, the name of the file to be compressed \ne.g \"input.txt\".\n\n");
		return 0;
	}
	else
	{
		strcpy(fileName, argv[1]);
	}

	FILE *outputFilePointer;
	outputFilePointer = fopen("compressed.dat", "ab+");

	//Next line allocates the memory needed for the dictionary
	dictionaryEntry *dictionary = (dictionaryEntry*)malloc((sizeof(dictionaryEntry)) * (arraySize));

	FILE *inputFilePointer = getInputFilePointer(fileName);

	//Next line allocates the memory needed for inputFile to size of inputFile
	char *inputFile = (char*)malloc(fileSize(inputFilePointer));
	fgets(inputFile, fileSize(inputFilePointer), inputFilePointer); //Saves the contents of the file to inputFile

	char currStrPlusNextChar[50] = { '\0' };
	int count = 0;
	char nextCharacter = inputFile[0];
	char currentString[50] = { '\0' };

	intitaliseDictionary(dictionary);

	while (inputFile[count] != '\0') //Main algorithm logic within this loop
	{
		nextCharacter = inputFile[count]; //Gets the next character from Input
		strcpy(currStrPlusNextChar, currentString); //Copies currentString to currStrPlusNextChar
		currStrPlusNextChar[strlen(currentString)] = nextCharacter; //Appends nextCharacter to currStrPlusNextChar


		if (isInDictionary(currStrPlusNextChar, dictionary) == true) //if dictionary contains currentString + nextCharacter
		{
			strcpy(currentString, currStrPlusNextChar); //Sets currentString + nextCharacter to currentString
		}
		else
		{
			addToOutput(currentString, dictionary, outputFilePointer); //Adds current string to output
			addToDictionary(currStrPlusNextChar, dictionary); //Adds currentString + nextCharacter to dictionary
			setToNull(currentString); //Next two lines set the currentString to nextCharacter
			currentString[0] = nextCharacter;
		}
		count++;
	}
	addToOutput(currentString, dictionary, outputFilePointer); //Adds current string to output
	addToOutput("0", dictionary, outputFilePointer); //Appends the file with the EOF charater

	fclose(outputFilePointer);
	printf("The file has been successfully compressed.\n");
	free(dictionary);
	free(inputFile);
	fclose(inputFilePointer);

	return 0;
}

bool isInDictionary(char * character, dictionaryEntry * dictionary) //Checks if a given character is in the dictionary
{
	for (int i = 0; i < arraySize; i++) //Loops through all dictionary entries
	{
		if (strcmp(dictionary[i].character, character) == 0) //Compares the given string to each dictionary entry
		{
			return true;
		}
	}

	return false;
}

void addToDictionary(char * character, dictionaryEntry * dictionary) //Adds a given character string to the dictionary
{
	for (int i = 29; i < arraySize; i++)
	{
		if (dictionary[i].character[0] == '\0') //Finds empty dictionary entry
		{
			strcpy(dictionary[i].character, character); //Sets dictionary entry character as given character
			dictionary[i].codeword = i;
			return;
		}
	}
}

void addToOutput(char * character, dictionaryEntry * dictionary, FILE * outputFilePointer) //adds a given character array to the output array
{
	for (int i = 0; i < arraySize; i++) //Loops through dictionary
	{
		if (strcmp(dictionary[i].character, character) == 0) //When the dictionary entry matching the given string is found
		{
			short codeToWrite = dictionary[i].codeword;

			fwrite(&codeToWrite, sizeof(short), 1, outputFilePointer); //Writes the output file in binary

			return;
		}
	}
}

void intitaliseDictionary(dictionaryEntry * dictionary) //Initialises the dictionary with the default values
{
	char characterStart = 'A';

	for (int i = 0; i < arraySize; i++) //Initialises all dictionary.characters to NULL
	{
		for (int x = 0; x < 50; x++)
		{
			dictionary[i].character[x] = (char)'\0';
		}
	}

	//The following lines add the default symbols to the dictionary
	dictionary[0].codeword = 0;
	dictionary[0].character[0] = '0';

	dictionary[27].codeword = 27;
	dictionary[27].character[0] = ' ';

	dictionary[28].codeword = 28;
	dictionary[28].character[0] = '.';

	for (int i = 1; i <= 26; i++) //Initialises the capital letters
	{
		dictionary[i].codeword = i;
		dictionary[i].character[0] = (characterStart);
		characterStart++;
	}
}

void setToNull(char * string) //Sets all characters in an array to null
{
	int i = 0;
	while (string[i] != '\0')
	{
		string[i] = '\0';
		i++;
	}
}

FILE * getInputFilePointer(char * fileName) //Finds, opens, reads, and closes the file and saves contents to memory
{
	FILE *inputFilePointer;

	do //This loop continues until a file has been read successfully
	{
		inputFilePointer = fopen(fileName, "r");

		if (inputFilePointer == NULL)
		{
			printf("Unable to read file, please try again or press CTRL + C to exit.\n\n");
		}

	} while (inputFilePointer == NULL);

	//char *inputString = (char*)malloc(fileSize(inputFilePointer));

	//fgets(inputString, 500, inputFilePointer); //Loads the input string into memory

	return inputFilePointer;
}

int fileSize(FILE * inputFilePointer)
{
	int previousPostion = ftell(inputFilePointer); //Gets the start position of the pointer
	fseek(inputFilePointer, 0L, SEEK_END); //Finds of the end of the file
	int sizeOfFile = ftell(inputFilePointer); //Sets sizeOfFile as size of the file
	fseek(inputFilePointer, previousPostion, SEEK_SET); //Sets pointer to start position of file
	return sizeOfFile;
}
