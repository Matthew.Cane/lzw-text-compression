#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct _dictionaryEntry
{
	unsigned int codeword : 8;
	char character[50];
} dictionaryEntry;
	
bool isInDictionary(char * character, dictionaryEntry * dictionary);
void addToDictionary(char *character, dictionaryEntry *dictionary);
void addToOutput(char *character, dictionaryEntry *dictionary, char *outputFile);
void intitaliseDictionary(dictionaryEntry *dictionary);
void setToNull(char *string);

int main()
{
	dictionaryEntry dictionary[256];
	char outputFile[500] = { '\0' };
	char currStrPlusNextChar[50] = { '\0' };
	int count = 0;
	char inputFile[200] = "THERE ARE ONLY TWO WAYS TO LIVE YOUR LIFE. ONE IS AS THOUGH NOTHING IS A MIRACLE. THE OTHER IS AS THOUGH EVERYTHING IS A MIRACLE.";

	intitaliseDictionary(dictionary);
	char nextCharacter = inputFile[0];
	char currentString[50] = { '\0' };
	
	while (inputFile[count] != '\0') //Main algorithm logic within this loop
	{
		nextCharacter = inputFile[count]; //Gets the next character from Input
		strcpy(currStrPlusNextChar, currentString); //Copies currentString to currStrPlusNextChar
		currStrPlusNextChar[strlen(currentString)] = nextCharacter; //Appends nextCharacter to currStrPlusNextChar
		

		if (isInDictionary(currStrPlusNextChar, dictionary) == true) //if dictionary contains currentString + nextCharacter
		{
			strcpy(currentString, currStrPlusNextChar); //Sets currentString + nextCharacter to currentString
		}
		else
		{
			addToOutput(currentString, dictionary, outputFile); //Adds current string to output
			addToDictionary(currStrPlusNextChar, dictionary); //Adds currentString + nextCharacter to dictionary
			setToNull(currentString); //Next two lines set the currentString to nextCharacter
			currentString[0] = nextCharacter;
		}
		count++;
	}
	addToOutput(currentString, dictionary, outputFile); //Adds current string to output
	addToOutput("0", dictionary, outputFile); //Appends the file with the EOF charater

	printf("%s\n", outputFile);

	return 0;
}

bool isInDictionary(char * character, dictionaryEntry * dictionary) //Checks if a given character is in the dictionary
{
	for (int i = 0; i < 256; i++) //Loops through all dictionary entries
	{
		if (strcmp(dictionary[i].character, character) == 0) //Compares the given string to each dictionary entry
		{
			return true;
		}
	}

	return false;
}

void addToDictionary(char * character, dictionaryEntry * dictionary) //Adds a given character string to the dictionary
{
	for (int i = 29; i < 256; i++)
	{
		if (dictionary[i].character[0] == '\0') //Finds empty dictionary entry
		{
			strcpy(dictionary[i].character, character); //Sets dictionary entry character as given character
			dictionary[i].codeword = i;
			return;
		}
	}
}

void addToOutput(char * character, dictionaryEntry * dictionary, char *outputFile) //adds a given character array to the output array
{
	for (int i = 0; i < 256; i++) //Loops through dictionary
	{
		if (strcmp(dictionary[i].character, character) == 0) //When the dictionary entry matching the given string is found
		{
			char charWord[8] = { '\0' };
			sprintf(charWord, "%d", dictionary[i].codeword); //Converts the int code codeword to char charWord
			
			strcat(outputFile, "<");
			strcat(outputFile, charWord);
			strcat(outputFile, ">");
			
			return;
		}
	}
}

void intitaliseDictionary(dictionaryEntry * dictionary) //Initialises the dictionary with the default values
{
	char characterStart = 'A';

	for (int i = 0; i < 256; i++) //Initialises all dictionary.characters to NULL
	{
		for (int x = 0; x < 50; x++)
		{
			dictionary[i].character[x] = (char)'\0';
		}
	}

	dictionary[0].codeword = 0;
	dictionary[0].character[0] = '0';

	dictionary[27].codeword = 27;
	dictionary[27].character[0] = ' ';

	dictionary[28].codeword = 28;
	dictionary[28].character[0] = '.';

	for (int i = 1; i <= 26; i++) //Initialises the capital letters
	{
		dictionary[i].codeword = i;
		dictionary[i].character[0] = (characterStart);
		characterStart++;
	}
}

void setToNull(char * string) //Sets all characters in an array to null
{
	int i = 0;
	while (string[i] != '\0')
	{
		string[i] = '\0';
		i++;
	}
}