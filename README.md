# LZW Text Compression

This is a short text compression program written in C that utilises the LZW compression algorithm.

This program was written as part of my first year of studies in Computing at UWE Bristol.
